package org.bsc.mancala;

/**
 * @author Danil Suits (danil@vast.com)
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This test is used to ensure that JUnit is wired up
 * correctly, that it can find this class, identify the
 * test within it, and run that test, find the test subject
 * and report the result.
 * comment
 *
 * @author Danil Suits (danil@vast.com)
 */
public class CalibrationTest {
    // The motivation for this enum is simply to get
    // nice error messages if the production component
    // doesn't return the answer that we want.
    enum Result {PASS, FAIL}

    @Test
    public void verify_initial_board_state() {
        int[] array = {4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0};
        {
            int pocket = 1;
            assertEquals(4, countOfStones(array, pocket));
        }
        for (int pocket = 0; pocket < 6; ++pocket) {
            assertEquals(4, countOfStones(array, pocket));
        }
        assertEquals(0, countOfStones(array, 6));
        assertEquals(0, countOfStones(array, 13));
    }

    @Test
    public void test_first_move_is_first_pocket() {
        int[] array = {4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0};
        int[] new_array = move(array, 0);
        assertEquals(0, countOfStones(new_array, 0));
        assertEquals(5, countOfStones(new_array, 1));
        assertEquals(5, countOfStones(new_array, 2));
        assertEquals(5, countOfStones(new_array, 3));
        assertEquals(5, countOfStones(new_array, 4));
        assertEquals(4, countOfStones(new_array, 5));
    }

    @Test
    public void test_first_move_is_second_pocket() {
        int[] array = {4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0};
        int[] new_array = move(array, 1);

        assertEquals(4, countOfStones(new_array, 0));
        assertEquals(0, countOfStones(new_array, 1));
        assertEquals(5, countOfStones(new_array, 2));
        assertEquals(5, countOfStones(new_array, 3));
        assertEquals(5, countOfStones(new_array, 4));
        assertEquals(5, countOfStones(new_array, 5));

        assertEquals(0, countOfStones(new_array, 6));
    }

    @Test
    public void test_second_move_is_second_pocket_after_first_pocket() {
        int[] array = {0, 5, 5, 5, 5, 4, 0, 4, 4, 4, 4, 4, 4, 0};
        int[] new_array = move(array, 1);

        assertEquals(0, countOfStones(new_array, 0));
        assertEquals(0, countOfStones(new_array, 1));
        assertEquals(6, countOfStones(new_array, 2));
        assertEquals(6, countOfStones(new_array, 3));
        assertEquals(6, countOfStones(new_array, 4));
        assertEquals(5, countOfStones(new_array, 5));

        assertEquals(1, countOfStones(new_array, 6));

        assertEquals(4, countOfStones(new_array, 7));
    }

    @Test
    public void test_second_move_is_third_pocket_after_first_pocket() {
        int[] array = {0, 5, 5, 5, 5, 4, 0, 4, 4, 4, 4, 4, 4, 0};
        int[] new_array = move(array, 2);

        assertEquals(0, countOfStones(new_array, 0));
        assertEquals(5, countOfStones(new_array, 1));
        assertEquals(0, countOfStones(new_array, 2));
        assertEquals(6, countOfStones(new_array, 3));
        assertEquals(6, countOfStones(new_array, 4));
        assertEquals(5, countOfStones(new_array, 5));

        assertEquals(1, countOfStones(new_array, 6));

        assertEquals(5, countOfStones(new_array, 7));
    }

    @Test
    public void test_skip_the_opponents_mancala() {
        int[] array = {0, 5, 5, 4, 1, 9, 0, 4, 4, 4, 4, 4, 4, 0};
        int[] new_array = move(array, 5);

        assertEquals(1, countOfStones(new_array, 6));
        assertEquals(0, countOfStones(new_array, 13));

        assertEquals(1, countOfStones(new_array, 0));
        assertEquals(6, countOfStones(new_array, 1));
    }

    @Test
    public void test_land_in_empty_pocket_on_my_side() {
        int[] array = {1, 0, 4, 4, 4, 4, 0, 4, 4, 4, 4, 0, 4, 11};
        int[] new_array = move(array, 0);
//        int[] array = {0, 0, 4, 4, 4, 4, 1, 4, 4, 4, 4, 0, 4, 11};

        assertEquals(0, countOfStones(new_array, 0));
        assertEquals(0, countOfStones(new_array, 1));
        assertEquals(1, countOfStones(new_array, 6));

    }

    @Test
    public void test_land_in_empty_pocket_on_my_side_win_extra_stones() {
        int[] array = {1, 0, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 0, 11};
        int[] new_array = move(array, 0);
//        int[] array = {0, 0, 4, 4, 4, 4, 5, 4, 4, 4, 4, 0, 0, 11};

        assertEquals(0, countOfStones(new_array, 0));
        assertEquals(0, countOfStones(new_array, 1));
        assertEquals(5, countOfStones(new_array, 6));

    }

    private int[] move(int[] array, int pocket) {
        int stones = array[pocket];
        array[pocket] = 0; // where did the stones go?
        for (int x = pocket + 1; x < pocket + stones; ++x) {
            array[x % 13] += 1;
        }
        // We need special rules for the last stone.
        // IF it's the last stone
        {
            int x = pocket + stones;
            if (0 == array[x % 13]) {
                array[6] += 1;
                // TODO: take stones from opponent pocket!
            } else {
                array[x % 13] += 1;
            }
        }
        return array;
    }

    int countOfStones(int[] array, int pocket) {
        return array[pocket];
    }

    @Test
    public void the_test_subject_should_return_the_first_argument() {
        assertEquals(
                Result.PASS,
                TestSubject.testResult(
                        Result.PASS,
                        Result.FAIL
                )
        );
    }
}

class TestSubject {
    static <T> T testResult(T pass, T fail) {
        return pass;
    }
}

